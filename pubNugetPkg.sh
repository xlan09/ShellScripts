#!/bin/bash
# Copy nuget packages from ./_artifacts/Packages to cache and to \\wilfile02\GroupShared\PNC\BuildPackages
echo "Make sure you are in folder Shared, SRE or System-Controls"
echo "Current folder is:"
pwd
echo "----------------------"
currentGitBranch=$(git rev-parse --abbrev-ref HEAD)
echo $currentGitBranch
echo "----------------------"
dstPath="//wilfile02/GroupShared/PNC/BuildPackages/$currentGitBranch"
echo "Start copying nuget package to $dstPath..."
mkdir -p $dstPath
cp -fr /cygdrive/c/Users/xlan/AppData/Local/NuGet/Cache/* $dstPath
echo "Copy are done!"
