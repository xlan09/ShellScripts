#!bin/bash
scp /cygdrive/c/dev/Scripts/CokeScripts/simba_init.py ddade@192.168.127.132:/home/ddade/simbaSim #scp is ssh copy command
ssh ddade@192.168.127.132 rm /home/ddade/simbaSim/\{\*.log,Log_*.txt\}
ssh ddade@192.168.127.132 "cd /home/ddade/simbaSim && python simba_init.py"  #must be in the same line

# set up MSUI and matrix rover
cp /cygdrive/c/dev/Scripts/CokeScripts/Symbotic.MatrixRover.Client.exe.config  /cygdrive/c/dev/SRE/Symbotic.MatrixRover/Symbotic.MatrixRover.Client/bin/Debug
cp /cygdrive/c/dev/Scripts/CokeScripts/Overrides.config /cygdrive/c/dev/system-controls/Symbotic.Peregrine.MatrixSelectUI/bin/Debug

# remove logs
now=$(date +"%Y-%m-%d")
rm -fr /cygdrive/c/cps/Engine/$now/*.*
