#!/bin/bash

date=$(pwd | grep -oP '.*es/\K.*$')
file="/home/$USER/IBsuspect$date.txt"

sum=0
if [[ -e "$file" ]]; then
  tempFile="/home/$USER/tempFile.txt"
  sed -e 's/\(.*txt\)\(.*Pick \)\(.* \)\(.*$\)/\1 \3/' "$file" > "$tempFile"
  while IFS=' ' read -r filename actionId line || [[ -n "$line" ]]; do
    if [[ "$line" =~ .*Sku.* ]]; then
      echo "grepping $actionId in $filename"
	    count=$(grep "$actionId.*CaseMoved" "$filename" -c)
	    echo "count=$count"
      sum=$(( $sum + $count ))
    fi
  done < "$tempFile"

  echo "sum is $sum"
  rm -f "$tempFile"
else
  echo "$file not exist"
  exit 1
fi
exit
